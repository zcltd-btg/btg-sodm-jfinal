package cn.zcltd.btg.sodm.exception;

/**
 * 没有列修改数据访问权限异常
 */
public class SODMColumnUpdateException extends SODMColumnException {
}
