package cn.zcltd.btg.sodm.exception;

/**
 * 没有行更新操作权限异常
 */
public class SODMRowUpdateException extends SODMRowException {
}
