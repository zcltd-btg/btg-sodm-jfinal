package cn.zcltd.btg.sodm.exception;

/**
 * 没有列查询数据访问权限异常
 */
public class SODMColumnSelectException extends SODMColumnException {
}
