package cn.zcltd.btg.sodm.exception;

/**
 * 没有行查询操作权限异常
 */
public class SODMRowSelectException extends SODMRowException {
}
