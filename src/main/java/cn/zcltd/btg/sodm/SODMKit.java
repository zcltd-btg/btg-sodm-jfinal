package cn.zcltd.btg.sodm;

import cn.zcltd.btg.sodm.exception.SODMException;

/**
 * sodm使用工具类
 */
public class SODMKit {

    /**
     * 认证insert
     *
     * @param sqlStr sql语句
     * @return 执行sql
     * @throws SODMException 异常
     */
    public static String insert(String sqlStr) throws SODMException {
        return null;
    }

    /**
     * 认证update
     *
     * @param sqlStr sql语句
     * @return 执行sql
     * @throws SODMException 异常
     */
    public static String update(String sqlStr) throws SODMException {
        return null;
    }

    /**
     * 认证delete
     *
     * @param sqlStr sql语句
     * @return 执行sql
     * @throws SODMException 异常
     */
    public static String delete(String sqlStr) throws SODMException {
        return null;
    }

    /**
     * 认证select
     *
     * @param sqlStr sql语句
     * @return 执行sql
     * @throws SODMException 异常
     */
    public static String select(String sqlStr) throws SODMException {
        return null;
    }
}