package cn.zcltd.btg.sodm.core.sqlparser.impl;

import cn.zcltd.btg.sodm.core.sqlparser.SQL;
import cn.zcltd.btg.sodm.core.sqlparser.SQLParser;
import cn.zcltd.btg.sodm.core.sqlparser.sqlo.SQLDeleteStandard;

import java.util.List;
import java.util.Map;

/**
 * SQL解析器：delete
 */
public class SQLParserDeleteStandard implements SQLParser {

    @Override
    public SQL parse(String sqlStr) {
        SQL sqlo = new SQLDeleteStandard();
        return sqlo;
    }

    @Override
    public SQL parse(String sqlStr, Map<String, Object> paramMap) {
        return null;
    }

    @Override
    public SQL parse(String sqlStr, List<Object> paramList) {
        return null;
    }

}
