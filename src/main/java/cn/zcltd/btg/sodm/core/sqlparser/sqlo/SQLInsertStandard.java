package cn.zcltd.btg.sodm.core.sqlparser.sqlo;

import cn.zcltd.btg.sodm.core.sqlparser.SQL;
import cn.zcltd.btg.sodm.core.sqlparser.xo.SQLXO;

import java.util.*;

/**
 * SQL对象：insert
 */
public class SQLInsertStandard implements SQL {

    private String table; //表名
    private Map<String, Object> fieldsMap = new HashMap<String, Object>(); //字段值映射
    private List<String> fields = new ArrayList<String>(); //字段列表
    private List<Object> values = new ArrayList<Object>(); //值列表

    @Override
    public String generate() {
        StringBuffer sqlBf = new StringBuffer("insert into ");
        sqlBf.append(this.table);
        sqlBf.append("(");

        StringBuffer filedsBf = new StringBuffer();
        StringBuffer valuesBf = new StringBuffer();
        Iterator<Map.Entry<String, Object>> entryIterator = this.fieldsMap.entrySet().iterator();
        while (entryIterator.hasNext()) {
            Map.Entry<String, Object> entry = entryIterator.next();
            String field = entry.getKey();
            Object value = entry.getValue();

            filedsBf.append(field);
            filedsBf.append(",");

            valuesBf.append(value + ",");
        }

        sqlBf.append(filedsBf.substring(0, filedsBf.length() - 1));
        sqlBf.append(") values (");
        sqlBf.append(valuesBf.substring(0, valuesBf.length() - 1));
        sqlBf.append(")");

        return sqlBf.toString();
    }

    @Override
    public SQLXO generateSQLXO() {
        SQLXO sqlxo = new SQLXO();

        StringBuffer sqlBf = new StringBuffer("insert into ");
        sqlBf.append(this.table);
        sqlBf.append("(");

        StringBuffer filedsBf = new StringBuffer();
        StringBuffer valuesBf = new StringBuffer();
        Iterator<Map.Entry<String, Object>> entryIterator = this.fieldsMap.entrySet().iterator();
        while (entryIterator.hasNext()) {
            Map.Entry<String, Object> entry = entryIterator.next();
            String field = entry.getKey();
            Object value = entry.getValue();

            filedsBf.append(field);
            filedsBf.append(",");

            valuesBf.append("?,");
            //sqlxo.setParam(field, SQLUtil.SQLValue2Param(value));
            sqlxo.setParam(field, value);
        }

        sqlBf.append(filedsBf.substring(0, filedsBf.length() - 1));
        sqlBf.append(") values (");
        sqlBf.append(valuesBf.substring(0, valuesBf.length() - 1));
        sqlBf.append(")");

        sqlxo.setSql(sqlBf.toString());

        return sqlxo;
    }

    /**
     * 添加一个字段
     *
     * @param field field
     */
    public void addField(String field) {
        this.fields.add(field);
        this.fieldsMap.put(field, null);
    }

    /**
     * 添加一个值
     *
     * @param value field
     */
    public void addValue(Object value) {
        this.values.add(value);
        this.fieldsMap.put(this.fields.get(this.values.size() - 1), value);
    }

    /**
     * 添加一个字段-值映射
     *
     * @param field field
     * @param value value
     */
    public void addMap(String field, Object value) {
        this.fields.add(field);
        this.values.add(value);
        this.fieldsMap.put(field, value);
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public Map<String, Object> getFieldsMap() {
        return fieldsMap;
    }

    public void setFieldsMap(Map<String, Object> fieldsMap) {
        this.fieldsMap = fieldsMap;
    }

    public List<String> getFields() {
        return fields;
    }

    public void setFields(List<String> fields) {
        this.fields = fields;
    }

    public List<Object> getValues() {
        return values;
    }

    public void setValues(List<Object> values) {
        this.values = values;
    }
}