package cn.zcltd.btg.sodm.core.config.cache;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * 权限认证规则细粒度缓存
 */
public class SODMCache {

    //用户权限配置信息
    public static Map<String, Map<String, Map<String, Set<SODMRule>>>> config = new HashMap<String, Map<String, Map<String, Set<SODMRule>>>>();

    /**
     * 根据配置将数据加载到缓存
     */
    public static void init() {
        //todo:这里使用不同dao获取用户权限配置，加载到config中，将用户、用户组、父用户组全部转化为不重复的用户权限规则列表
    }

    /**
     * 获取指定用户对指定表指定SODM类型的权限规则
     *
     * @param userId    用户id
     * @param tableName 表名
     * @param SODMType  SODM类型
     * @return 规则列表
     */
    public static Set<SODMRule> getRule(String userId, String tableName, String SODMType) {
        return config.get(userId).get(tableName).get(SODMType);
    }
}
