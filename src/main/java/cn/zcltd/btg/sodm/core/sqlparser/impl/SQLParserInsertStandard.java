package cn.zcltd.btg.sodm.core.sqlparser.impl;

import cn.zcltd.btg.sodm.core.config.cache.TableCache;
import cn.zcltd.btg.sodm.core.config.xo.TableField;
import cn.zcltd.btg.sodm.core.sqlparser.SQL;
import cn.zcltd.btg.sodm.core.sqlparser.SQLParser;
import cn.zcltd.btg.sodm.core.sqlparser.sqlo.SQLInsertStandard;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * SQL解析器：insert
 */
public class SQLParserInsertStandard implements SQLParser {

    @Override
    public SQL parse(String sqlStr) {
        SQLInsertStandard sqlo = new SQLInsertStandard();
        sqlStr = sqlStr.trim();

        //获取关键坐标
        int idxInto = sqlStr.indexOf("into");
        int idxFirstKh = sqlStr.indexOf("(");
        int idxValues = sqlStr.indexOf("values");
        int idxFirstKhAfterValues = sqlStr.indexOf("(", idxValues);

        //是否包含字段列表
        boolean hasFileds = idxValues > idxFirstKh;

        /*
            解析表名
         */
        String tableName;
        if (hasFileds) {
            tableName = sqlStr.substring(idxInto + 4, idxFirstKh).trim();
        } else {
            tableName = sqlStr.substring(idxInto + 4, idxValues).trim();
        }
        sqlo.setTable(tableName);

        /*
            解析插入字段
         */
        if (hasFileds) {
            String filedsStr = sqlStr.substring(idxFirstKh + 1, idxValues - 1).trim();
            filedsStr = filedsStr.substring(0, filedsStr.length() - 1);
            String[] fileds = filedsStr.split(",");
            for (String field : fileds) {
                sqlo.addField(field.trim());
            }
        } else {
            List<TableField> fields = TableCache.getFields(tableName);
            for (TableField field : fields) {
                if (field.isPk() && field.isAuto()) {
                    continue;
                }
                sqlo.addField(field.getCode());
            }
        }

        /*
            解析值
         */
        String valuesStr = sqlStr.substring(idxFirstKhAfterValues + 1, sqlStr.length() - 1);
        String[] values = valuesStr.split(",");
        for (String value : values) {
            sqlo.addValue(value.trim());
        }

        return sqlo;
    }

    @Override
    public SQL parse(String sqlStr, Map<String, Object> paramMap) {
        //参数处理，将sql中的占位符替换为参数
        Iterator<Map.Entry<String, Object>> entryIterator = paramMap.entrySet().iterator();
        while (entryIterator.hasNext()) {
            Map.Entry<String, Object> entry = entryIterator.next();
            //sqlStr = sqlStr.replace("@" + entry.getKey(), SQLUtil.Param2SQLValue(entry.getValue()));
            sqlStr = sqlStr.replace("@" + entry.getKey(), entry.getValue().toString());
        }
        return parse(sqlStr);
    }

    @Override
    public SQL parse(String sqlStr, List<Object> paramList) {
        //参数处理，将sql中的占位符替换为参数
        for (Object param : paramList) {
            //sqlStr = sqlStr.replaceFirst("\\?", SQLUtil.Param2SQLValue(param));
            sqlStr = sqlStr.replaceFirst("\\?", param.toString());
        }
        return parse(sqlStr);
    }

}
