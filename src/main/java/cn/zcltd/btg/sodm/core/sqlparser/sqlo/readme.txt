此包里的SQL对象实现了SQL标记接口，此处提供标准SQL的实现，可自定义扩展其他类型的SQL对象，
两种扩展方法：1、实现SQL接口 2、继承SQLXXX类，重写父类方法
如：

对于MySQL有：
SQLInsertMySQL
SQLUpdateMySQL
SQLDeleteMySQL
SQLSelectMySQL

对于Oracle有：
SQLInsertOracle
SQLUpdateOracle
SQLDeleteOracle
SQLSelectOracle
