package cn.zcltd.btg.sodm.core.sqlparser.impl;

import cn.zcltd.btg.sodm.core.sqlparser.SQL;
import cn.zcltd.btg.sodm.core.sqlparser.SQLParser;
import cn.zcltd.btg.sodm.core.sqlparser.sqlo.SQLSelectStandard;

import java.util.List;
import java.util.Map;

/**
 * SQL解析器：select
 */
public class SQLParserSelectStandard implements SQLParser {

    @Override
    public SQL parse(String sqlStr) {
        SQL sqlo = new SQLSelectStandard();
        return sqlo;
    }

    @Override
    public SQL parse(String sqlStr, Map<String, Object> paramMap) {
        return null;
    }

    @Override
    public SQL parse(String sqlStr, List<Object> paramList) {
        return null;
    }

}
