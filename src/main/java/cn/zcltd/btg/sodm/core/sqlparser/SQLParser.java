package cn.zcltd.btg.sodm.core.sqlparser;

import java.util.List;
import java.util.Map;

/**
 * SQL解析器接口
 */
public interface SQLParser {

    /**
     * 解析sql语句(标准sql语句，不带变量绑定)
     *
     * @param sqlStr sql语句
     * @return SQL对象
     */
    public SQL parse(String sqlStr);

    /**
     * 解析sql语句(带变量绑定的sql语句，参数为Map格式，sql语句占位符为@filed)
     *
     * @param sqlStr   sql语句
     * @param paramMap 参数列表
     * @return SQL对象
     */
    public SQL parse(String sqlStr, Map<String, Object> paramMap);

    /**
     * 解析sql语句(带变量绑定的sql语句，参数为List格式，sql语句占位符为?)
     *
     * @param sqlStr    sql语句
     * @param paramList 参数列表
     * @return SQL对象
     */
    public SQL parse(String sqlStr, List<Object> paramList);

}
