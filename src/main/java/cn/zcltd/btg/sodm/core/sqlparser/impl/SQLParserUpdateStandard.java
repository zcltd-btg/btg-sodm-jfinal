package cn.zcltd.btg.sodm.core.sqlparser.impl;

import cn.zcltd.btg.sodm.core.sqlparser.SQL;
import cn.zcltd.btg.sodm.core.sqlparser.SQLParser;
import cn.zcltd.btg.sodm.core.sqlparser.sqlo.SQLUpdateStandard;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * SQL解析器：update
 */
public class SQLParserUpdateStandard implements SQLParser {

    @Override
    public SQL parse(String sqlStr) {
        SQLUpdateStandard sqlo = new SQLUpdateStandard();
        sqlStr = sqlStr.trim();

        //获取关键坐标
        int idxUpdate = sqlStr.indexOf("update");
        int idxSet = sqlStr.indexOf("set");
        int idxWhere = sqlStr.indexOf("where");

         /*
            解析表名
         */
        String tableName = sqlStr.substring(idxUpdate + 6, idxSet).trim();
        sqlo.setTable(tableName);

         /*
            解析字段更新
         */
        String setsStr = sqlStr.substring(idxSet + 3, idxWhere);
        String[] setStr = setsStr.split(",");
        for (String sStr : setStr) {
            String[] setConfig = sStr.split("=");
            sqlo.addMap(setConfig[0].trim(), setConfig[1].trim());
        }

        /*
            解析where子句
         */
        StringBuffer whereSqlSb = new StringBuffer();
        String whereStr = sqlStr.substring(idxWhere + 5).trim();
        String[] wsStr = whereStr.split("and");
        for (String wStr : wsStr) {
            String[] whereConfig = wStr.split("=");
            whereSqlSb.append(whereConfig[0].trim());
            whereSqlSb.append(" = ");
            whereSqlSb.append("?");
            //sqlo.addValueNoField(SQLUtil.SQLValue2Param(whereConfig[1].trim()));
            sqlo.addValueNoField(whereConfig[1].trim());
        }

        sqlo.setWhereSql(whereSqlSb.toString());

        return sqlo;
    }

    @Override
    public SQL parse(String sqlStr, Map<String, Object> paramMap) {
        //参数处理，将sql中的占位符替换为参数
        Iterator<Map.Entry<String, Object>> entryIterator = paramMap.entrySet().iterator();
        while (entryIterator.hasNext()) {
            Map.Entry<String, Object> entry = entryIterator.next();
            //sqlStr = sqlStr.replace("@" + entry.getKey(), SQLUtil.Param2SQLValue(entry.getValue()));
            sqlStr = sqlStr.replace("@" + entry.getKey(), entry.getValue().toString());
        }
        return parse(sqlStr);
    }

    @Override
    public SQL parse(String sqlStr, List<Object> paramList) {
        //参数处理，将sql中的占位符替换为参数
        for (Object param : paramList) {
            //sqlStr = sqlStr.replaceFirst("\\?", SQLUtil.Param2SQLValue(param));
            sqlStr = sqlStr.replaceFirst("\\?", param.toString());
        }
        return parse(sqlStr);
    }

}
