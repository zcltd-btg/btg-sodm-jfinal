package cn.zcltd.btg.sodm.core.sqlparser.sqlo;

import cn.zcltd.btg.sodm.core.sqlparser.SQL;
import cn.zcltd.btg.sodm.core.sqlparser.xo.SQLXO;
import cn.zcltd.btg.sutil.EmptyUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * SQL对象：select
 */
public class SQLSelectStandard implements SQL {

    private String table; //表名
    private List<String> fields = new ArrayList<String>(); //字段列表
    private String whereSql; //条件字符串
    private List<Object> params = new ArrayList<Object>(); //参数列表

    @Override
    public String generate() {
        StringBuffer sqlBf = new StringBuffer("select ");

        StringBuffer filedsBf = new StringBuffer();
        for (String field : this.fields) {
            filedsBf.append(field + ",");
        }
        sqlBf.append(filedsBf.substring(0, filedsBf.length() - 1));

        sqlBf.append(" from ");
        sqlBf.append(this.table);

        String whereSqlNow = this.whereSql;
        for (Object param : this.params) {
            whereSqlNow = whereSqlNow.replace("?", param.toString());
        }
        if (EmptyUtil.isNotEmpty(whereSqlNow)) {
            sqlBf.append(" where " + whereSqlNow);
        }

        return sqlBf.toString();
    }

    @Override
    public SQLXO generateSQLXO() {
        SQLXO sqlxo = new SQLXO();

        StringBuffer sqlBf = new StringBuffer("select ");

        StringBuffer filedsBf = new StringBuffer();
        for (String field : this.fields) {
            filedsBf.append(field + ",");
        }
        sqlBf.append(filedsBf.substring(0, filedsBf.length() - 1));

        sqlBf.append(" from ");
        sqlBf.append(this.table);

        if (EmptyUtil.isNotEmpty(this.whereSql)) {
            sqlBf.append(" where " + this.whereSql);
        }

        sqlxo.setSql(sqlBf.toString());

        sqlxo.addParam(this.params);

        return sqlxo;
    }

    /**
     * 添加一个字段
     *
     * @param field field
     */
    public void addField(String field) {
        this.fields.add(field);
    }

    /**
     * 添加一个值
     *
     * @param value value
     */
    public void addValueNoField(Object value) {
        this.params.add(value);
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public List<String> getFields() {
        return fields;
    }

    public void setFields(List<String> fields) {
        this.fields = fields;
    }

    public String getWhereSql() {
        return whereSql;
    }

    public void setWhereSql(String whereSql) {
        this.whereSql = whereSql;
    }

    public List<Object> getParams() {
        return params;
    }

    public void setParams(List<Object> params) {
        this.params = params;
    }
}