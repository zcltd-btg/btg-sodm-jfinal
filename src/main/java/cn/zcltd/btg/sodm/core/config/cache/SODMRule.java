package cn.zcltd.btg.sodm.core.config.cache;

/**
 * SODM规则
 */
public class SODMRule {

    /*
        SODM权限对象类型
     */
    public static final String SODM_TYPE_COLUMN = "column"; //列(字段)
    public static final String SODM_TYPE_ROW = "row"; //行(记录)

    private String sodmType; //SODM权限对象类型
    private String target;//权限主体，如列名、行规则
    private String grant; //权限配置，使用四位二进制分别表示insert、update、delete、select

    public String getSodmType() {
        return sodmType;
    }

    public void setSodmType(String sodmType) {
        this.sodmType = sodmType;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getGrant() {
        return grant;
    }

    public void setGrant(String grant) {
        this.grant = grant;
    }
}
