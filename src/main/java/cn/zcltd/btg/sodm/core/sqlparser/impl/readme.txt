此包里的SQLParser子对象对象实现了SQLParserXXXX接口，此处提供标准SQL的实现，可自定义扩展其他类型的SQLParser，
两种扩展方法：1、实现SQLParserXXXX接口 2、继承SQLParserXXXStandard类，重写父类方法
如：

对于MySQL有：
SQLParserInsertMySQL
SQLParserUpdateMySQL
SQLParserDeleteMySQL
SQLParserSelectMySQL

对于Oracle有：
SQLParserInsertOracle
SQLParserUpdateOracle
SQLParserDeleteOracle
SQLParserSelectOracle
