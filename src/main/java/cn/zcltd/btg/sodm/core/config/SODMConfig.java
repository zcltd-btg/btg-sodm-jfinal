package cn.zcltd.btg.sodm.core.config;

import cn.zcltd.btg.sodm.core.config.cache.SODMCache;
import cn.zcltd.btg.sodm.core.config.cache.SODMRule;

import java.util.Set;

/**
 * 权限认证规则
 */
public class SODMConfig {

    /**
     * 获取指定用户对指定表指定SODM类型的权限规则
     *
     * @param userId    用户id
     * @param tableName 表名
     * @param SODMType  SODM类型
     * @return 规则列表
     */
    public static Set<SODMRule> getRule(String userId, String tableName, String SODMType) {
        return SODMCache.getRule(userId, tableName, SODMType);
    }
}
