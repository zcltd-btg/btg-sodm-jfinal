package cn.zcltd.btg.sodm.core.config.xo;

/**
 * 表字段
 */
public class TableField {
    private String code;
    private String name;
    private boolean isPk = false;
    private boolean isAuto = false;

    public TableField(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public TableField(String code, String name, boolean isPk) {
        this.code = code;
        this.name = name;
        this.isPk = isPk;
    }

    public TableField(String code, String name, boolean isPk, boolean isAuto) {
        this.code = code;
        this.name = name;
        this.isPk = isPk;
        this.isAuto = isAuto;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isPk() {
        return isPk;
    }

    public void setPk(boolean isPk) {
        this.isPk = isPk;
    }

    public boolean isAuto() {
        return isAuto;
    }

    public void setAuto(boolean isAuto) {
        this.isAuto = isAuto;
    }
}
