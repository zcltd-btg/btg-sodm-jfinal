package cn.zcltd.btg.sodm.core.sqlparser;

import cn.zcltd.btg.sodm.core.sqlparser.xo.SQLXO;

/**
 * SQL对象标记接口
 */
public interface SQL {

    /**
     * 将sql解析后的对象重新生成sql语句
     *
     * @return String
     */
    public String generate();

    /**
     * 将sql解析后的对象重新生成sql承载对象
     *
     * @return SQLXO
     */
    public SQLXO generateSQLXO();
}