package cn.zcltd.btg.sodm.core.sqlparser.sqlo;

import cn.zcltd.btg.sodm.core.sqlparser.SQL;
import cn.zcltd.btg.sodm.core.sqlparser.xo.SQLXO;
import cn.zcltd.btg.sutil.EmptyUtil;

import java.util.*;

/**
 * SQL对象：update
 */
public class SQLUpdateStandard implements SQL {

    private String table; //表名
    private Map<String, Object> fieldsMap = new HashMap<String, Object>(); //字段值映射
    private List<String> fields = new ArrayList<String>(); //字段列表
    private List<Object> values = new ArrayList<Object>(); //值列表
    private String whereSql; //条件字符串

    @Override
    public String generate() {
        StringBuffer sqlBf = new StringBuffer("update ");
        sqlBf.append(this.table);
        sqlBf.append(" set ");

        StringBuffer setBf = new StringBuffer();
        Iterator<Map.Entry<String, Object>> entryIterator = this.fieldsMap.entrySet().iterator();
        while (entryIterator.hasNext()) {
            Map.Entry<String, Object> entry = entryIterator.next();
            String field = entry.getKey();
            Object value = entry.getValue();

            setBf.append(field + " = " + value + ",");
        }
        sqlBf.append(setBf.substring(0, setBf.length() - 1));

        String whereSqlNow = this.whereSql;
        for (Object value : this.values.subList(this.fields.size(), this.values.size())) {
            whereSqlNow = whereSqlNow.replace("?", value.toString());
        }
        if (EmptyUtil.isNotEmpty(whereSqlNow)) {
            sqlBf.append(" where " + whereSqlNow);
        }

        return sqlBf.toString();
    }

    @Override
    public SQLXO generateSQLXO() {
        SQLXO sqlxo = new SQLXO();

        StringBuffer sqlBf = new StringBuffer("update ");
        sqlBf.append(this.table);
        sqlBf.append(" set ");

        StringBuffer setBf = new StringBuffer();
        Iterator<Map.Entry<String, Object>> entryIterator = this.fieldsMap.entrySet().iterator();
        while (entryIterator.hasNext()) {
            Map.Entry<String, Object> entry = entryIterator.next();
            String field = entry.getKey();
            Object value = entry.getValue();

            setBf.append(field + " = ?,");
            //sqlxo.setParam(field, SQLUtil.SQLValue2Param(value));
            sqlxo.setParam(field, value);
        }
        sqlBf.append(setBf.substring(0, setBf.length() - 1));

        if (EmptyUtil.isNotEmpty(this.whereSql)) {
            sqlBf.append(" where " + this.whereSql);
        }

        sqlxo.setSql(sqlBf.toString());

        sqlxo.addParam(this.values.subList(this.fields.size(), this.values.size()));

        return sqlxo;
    }

    /**
     * 添加一个字段
     *
     * @param field field
     */
    public void addField(String field) {
        this.fields.add(field);
        this.fieldsMap.put(field, null);
    }

    /**
     * 添加一个值
     *
     * @param value value
     */
    public void addValue(Object value) {
        this.values.add(value);
        this.fieldsMap.put(this.fields.get(this.values.size() - 1), value);
    }

    /**
     * 添加一个值
     *
     * @param value value
     */
    public void addValueNoField(Object value) {
        this.values.add(value);
    }

    /**
     * 添加一个字段-值映射
     *
     * @param field field
     * @param value value
     */
    public void addMap(String field, Object value) {
        this.fields.add(field);
        this.values.add(value);
        this.fieldsMap.put(field, value);
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public Map<String, Object> getFieldsMap() {
        return fieldsMap;
    }

    public void setFieldsMap(Map<String, Object> fieldsMap) {
        this.fieldsMap = fieldsMap;
    }

    public List<String> getFields() {
        return fields;
    }

    public void setFields(List<String> fields) {
        this.fields = fields;
    }

    public List<Object> getValues() {
        return values;
    }

    public void setValues(List<Object> values) {
        this.values = values;
    }

    public String getWhereSql() {
        return whereSql;
    }

    public void setWhereSql(String whereSql) {
        this.whereSql = whereSql;
    }
}