package cn.zcltd.btg.sodm.core.sqlparser.xo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * sql承载对象
 */
public class SQLXO {

    private String sql;//sql语句

    private Map<String, Object> paramMap = new HashMap<String, Object>(); //动态参数列表Map形式

    private List<Object> paramList = new ArrayList<Object>(); //动态参数列表List形式

    /**
     * 设置参数值
     *
     * @param field 字段名称
     * @param value 值
     */
    public void setParam(String field, Object value) {
        this.paramMap.put(field, value);
        this.paramList.add(value);
    }

    /**
     * 设置whereValues
     *
     * @param whereValues whereValues
     */
    public void addParam(List<Object> whereValues) {
        for (Object value : whereValues) {
            this.paramList.add(value);
        }
    }

    /**
     * 根据字段名称，获取字段值
     *
     * @param field 字段名称
     * @return Object
     */
    public Object getParam(String field) {
        return this.paramMap.get(field);
    }

    /**
     * 根据下标，获取字段值
     *
     * @param index 下标
     * @return Object
     */
    public Object getParam(int index) {
        return this.paramList.get(index);
    }

    /**
     * 根据字段名称，获取int类型的字段值
     *
     * @param field 字段名称
     * @return int
     */
    public int getParam2Int(String field) {
        return Integer.parseInt(String.valueOf(getParam(field)));
    }

    /**
     * 根据下标，获取int类型的字段值
     *
     * @param index 下标
     * @return int
     */
    public int getParam2Int(int index) {
        return Integer.parseInt(String.valueOf(getParam(index)));
    }

    /**
     * 根据字段名称，获取double类型的字段值
     *
     * @param field 字段名称
     * @return double
     */
    public double getParam2Double(String field) {
        return Double.parseDouble(String.valueOf(getParam(field)));
    }

    /**
     * 根据下标，获取double类型的字段值
     *
     * @param index 下标
     * @return double
     */
    public double getParam2Double(int index) {
        return Double.parseDouble(String.valueOf(getParam(index)));
    }

    /**
     * 根据字段名称，获取boolean类型的字段值
     *
     * @param field 字段名称
     * @return boolean
     */
    public boolean getParam2Boolean(String field) {
        return Boolean.parseBoolean(String.valueOf(getParam(field)));
    }

    /**
     * 根据下标，获取boolean类型的字段值
     *
     * @param index 下标
     * @return boolean
     */
    public boolean getParam2Boolean(int index) {
        return Boolean.parseBoolean(String.valueOf(getParam(index)));
    }

    public String getSql() {
        return sql;
    }

    public void setSql(String sql) {
        this.sql = sql;
    }

    public Map<String, Object> getParamMap() {
        return paramMap;
    }

    public void setParamMap(Map<String, Object> paramMap) {
        this.paramMap = paramMap;
    }

    public List<Object> getParamList() {
        return paramList;
    }

    public void setParamList(List<Object> paramList) {
        this.paramList = paramList;
    }
}