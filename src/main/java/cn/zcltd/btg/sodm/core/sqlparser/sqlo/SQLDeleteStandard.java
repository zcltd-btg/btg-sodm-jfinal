package cn.zcltd.btg.sodm.core.sqlparser.sqlo;

import cn.zcltd.btg.sodm.core.sqlparser.SQL;
import cn.zcltd.btg.sodm.core.sqlparser.xo.SQLXO;
import cn.zcltd.btg.sutil.EmptyUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * SQL对象：delete
 */
public class SQLDeleteStandard implements SQL {

    private String table; //表名
    private String whereSql; //条件字符串
    private List<Object> params = new ArrayList<Object>(); //参数列表

    @Override
    public String generate() {
        StringBuffer sqlBf = new StringBuffer("delete from ");
        sqlBf.append(this.table);

        String whereSqlNow = this.whereSql;
        for (Object param : this.params) {
            whereSqlNow = whereSqlNow.replace("?", param.toString());
        }
        if (EmptyUtil.isNotEmpty(whereSqlNow)) {
            sqlBf.append(" where " + whereSqlNow);
        }

        return sqlBf.toString();
    }

    @Override
    public SQLXO generateSQLXO() {
        SQLXO sqlxo = new SQLXO();

        StringBuffer sqlBf = new StringBuffer("delete from ");
        sqlBf.append(this.table);

        if (EmptyUtil.isNotEmpty(this.whereSql)) {
            sqlBf.append(" where " + this.whereSql);
        }

        sqlxo.setSql(sqlBf.toString());

        sqlxo.addParam(this.params);

        return sqlxo;
    }

    /**
     * 添加一个值
     *
     * @param value value
     */
    public void addValueNoField(Object value) {
        this.params.add(value);
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public List<Object> getParams() {
        return params;
    }

    public void setParams(List<Object> params) {
        this.params = params;
    }

    public String getWhereSql() {
        return whereSql;
    }

    public void setWhereSql(String whereSql) {
        this.whereSql = whereSql;
    }
}