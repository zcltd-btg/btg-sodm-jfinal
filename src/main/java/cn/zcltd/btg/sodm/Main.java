package cn.zcltd.btg.sodm;

import cn.zcltd.btg.sodm.exception.SODMException;

/**
 * main测试
 */
public class Main {

    public static void main(String[] args) {
        try {
            System.out.println(SODMKit.insert("insert into test(id,name) values (1,'zhangsan')"));
        } catch (SODMException e) {
            throw new RuntimeException("权限不足");
        }
    }
}
