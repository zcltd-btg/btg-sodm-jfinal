package cn.zcltd.btg.sodm.core.sqlparser.sqlo;

import cn.zcltd.btg.sodm.core.sqlparser.xo.SQLXO;
import org.junit.Test;

/**
 * SQLSelectStandard Tester.
 */
public class SQLSelectStandardTest {

    /**
     * Method: generate()
     */
    @Test
    public void testGenerate() throws Exception {
        SQLSelectStandard sql = new SQLSelectStandard();
        sql.setTable("t_aaa");
        sql.addField("b");
        sql.addField("c");
        //sql.setWhereSql("a = 1");
        sql.setWhereSql("a = ?");
        sql.addValueNoField(1);
        String sqlStr = sql.generate();
        System.out.println(sqlStr);
    }

    /**
     * Method: generateSQLXO()
     */
    @Test
    public void testGenerateSQLXO() throws Exception {
        SQLSelectStandard sql = new SQLSelectStandard();
        sql.setTable("t_aaa");
        sql.addField("b");
        sql.addField("c");
        //sql.setWhereSql("a = 1");
        sql.setWhereSql("a = ?");
        sql.addValueNoField(1);
        SQLXO sqlxo = sql.generateSQLXO();
        System.out.println(sqlxo.getSql());
        System.out.println(sqlxo.getParamList());
    }

}
