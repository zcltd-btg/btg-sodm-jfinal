package cn.zcltd.btg.sodm.core.sqlparser.sqlo;

import cn.zcltd.btg.sodm.core.sqlparser.xo.SQLXO;
import org.junit.Test;

/**
 * SQLUpdateStandard Tester.
 */
public class SQLUpdateStandardTest {

    /**
     * Method: generate()
     */
    @Test
    public void testGenerate() throws Exception {
        SQLUpdateStandard sql = new SQLUpdateStandard();
        sql.setTable("t_aaa");
        sql.addField("b");
        sql.addValue("'bbbb'");
        sql.addMap("c", "'2016-04-29 12:30:21'");
        //sql.setWhereSql("a = 1");
        sql.setWhereSql("a = ?");
        sql.addValueNoField(1);
        String sqlStr = sql.generate();
        System.out.println(sqlStr);
    }

    /**
     * Method: generateSQLXO()
     */
    @Test
    public void testGenerateSQLXO() throws Exception {
        SQLUpdateStandard sql = new SQLUpdateStandard();
        sql.setTable("t_aaa");
        sql.addField("b");
        sql.addValue("'bbbb'");
        sql.addMap("c", "'2016-04-29 12:30:21'");
        sql.setWhereSql("a = ?");
        sql.addValueNoField(1);
        SQLXO sqlxo = sql.generateSQLXO();
        System.out.println(sqlxo.getSql());
        System.out.println(sqlxo.getParamList());
    }

}
