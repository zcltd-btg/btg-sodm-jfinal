package cn.zcltd.btg.sodm.core.sqlparser.sqlo;

import cn.zcltd.btg.sodm.core.sqlparser.xo.SQLXO;
import org.junit.Test;

/**
 * SQLDeleteStandard Tester.
 */
public class SQLDeleteStandardTest {

    /**
     * Method: generate()
     */
    @Test
    public void testGenerate() throws Exception {
        SQLDeleteStandard sql = new SQLDeleteStandard();
        sql.setTable("t_aaa");
        //sql.setWhereSql("a = 1");
        sql.setWhereSql("a = ?");
        sql.addValueNoField(1);
        String sqlStr = sql.generate();
        System.out.println(sqlStr);
    }

    /**
     * Method: generateSQLXO()
     */
    @Test
    public void testGenerateSQLXO() throws Exception {
        SQLDeleteStandard sql = new SQLDeleteStandard();
        sql.setTable("t_aaa");
        sql.setWhereSql("a = ?");
        sql.addValueNoField(1);
        SQLXO sqlxo = sql.generateSQLXO();
        System.out.println(sqlxo.getSql());
        System.out.println(sqlxo.getParamList());
    }

}
