package cn.zcltd.btg.sodm.core.sqlparser.sqlo;

import cn.zcltd.btg.sodm.core.sqlparser.xo.SQLXO;
import org.junit.Test;

/**
 * SQLInsertStandard Tester.
 */
public class SQLInsertStandardTest {

    /**
     * Method: generate()
     */
    @Test
    public void testGenerate() throws Exception {
        SQLInsertStandard sql = new SQLInsertStandard();
        sql.setTable("t_aaa");
        sql.addField("b");
        sql.addValue("'bbbb'");
        sql.addMap("c", "'2016-04-29 12:30:21'");
        String sqlStr = sql.generate();
        System.out.println(sqlStr);
    }

    /**
     * Method: generateSQLXO()
     */
    @Test
    public void testGenerateSQLXO() throws Exception {
        SQLInsertStandard sql = new SQLInsertStandard();
        sql.setTable("t_aaa");
        sql.addField("b");
        sql.addValue("'bbbb'");
        sql.addMap("c", "'2016-04-29 12:30:21'");
        SQLXO sqlxo = sql.generateSQLXO();
        System.out.println(sqlxo.getSql());
        System.out.println(sqlxo.getParamList());
    }

}
