package cn.zcltd.btg.sodm.core.sqlparser.impl;

import cn.zcltd.btg.sodm.core.sqlparser.SQL;
import cn.zcltd.btg.sodm.core.sqlparser.xo.SQLXO;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.CaseInsensitiveContainerFactory;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.dialect.MysqlDialect;
import com.jfinal.plugin.druid.DruidPlugin;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * SQLParserUpdateStandard Tester.
 */
public class SQLParserUpdateStandardTest {

    @BeforeClass
    public static void before() {
        //连接池
        DruidPlugin druidPlugin = new DruidPlugin("jdbc:mysql://127.0.0.1/test?characterEncoding=utf8&zeroDateTimeBehavior=convertToNull", "root", "admin123");
        druidPlugin.start();

        //ActiveRecord支持
        ActiveRecordPlugin arp = new ActiveRecordPlugin(druidPlugin);
        arp.setDialect(new MysqlDialect()); //设置方言
        arp.setShowSql(true);//显示sql
        arp.setContainerFactory(new CaseInsensitiveContainerFactory(true)); //忽略大小写
        arp.start();
    }

    /**
     * Method: parse(String sqlStr)
     */
    @Test
    public void testParseSqlStr() throws Exception {
        SQLParserUpdateStandard sqlParserUpdateStandard = new SQLParserUpdateStandard();
        SQL sql = sqlParserUpdateStandard.parse("  update        t_aaa    set     b   =      'asdfasfd'   ,  c  =     '2016-01-02'   where a =  1 ");

        //标准sql
        String sqlStr = sql.generate();
        System.out.println(sqlStr);
        Db.update(sqlStr);

        //变量绑定
        SQLXO sqlxo = sql.generateSQLXO();
        System.out.println(sqlxo.getSql());
        System.out.println(sqlxo.getParamList());

        Db.update(sqlxo.getSql(), sqlxo.getParamList().toArray());
    }

    /**
     * Method: parse(String sqlStr, Map<String, Object> paramMap)
     */
    @Test
    public void testParseForSqlStrParamMap() throws Exception {
        SQLParserUpdateStandard sqlParserUpdateStandard = new SQLParserUpdateStandard();

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("p0", "asdf");
        map.put("p1", "2016-04-29 12:35:34");
        //map.put("p1", DateUtil.getNowDate());
        map.put("p2", 1);

        SQL sql = sqlParserUpdateStandard.parse("  update        t_aaa    set     b   =      @p0   ,  c  =     @p1   where a =  @p2 ", map);

        //标准sql
        String sqlStr = sql.generate();
        System.out.println(sqlStr);
        Db.update(sqlStr);

        //变量绑定
        SQLXO sqlxo = sql.generateSQLXO();
        System.out.println(sqlxo.getSql());
        System.out.println(sqlxo.getParamList());

        Db.update(sqlxo.getSql(), sqlxo.getParamList().toArray());
    }

    /**
     * Method: parse(String sqlStr, List<Object> paramList)
     */
    @Test
    public void testParseForSqlStrParamList() throws Exception {
        SQLParserUpdateStandard sqlParserUpdateStandard = new SQLParserUpdateStandard();

        List<Object> list = new ArrayList<Object>();
        list.add("asdf");
        list.add("2016-04-29 12:35:34");
        //list.add(DateUtil.getNowDate());
        list.add(1);

        SQL sql = sqlParserUpdateStandard.parse("  update        t_aaa    set     b   =      ?   ,  c  =     ?   where a =  ? ", list);

        //标准sql
        String sqlStr = sql.generate();
        System.out.println(sqlStr);
        Db.update(sqlStr);

        //变量绑定
        SQLXO sqlxo = sql.generateSQLXO();
        System.out.println(sqlxo.getSql());
        System.out.println(sqlxo.getParamList());

        Db.update(sqlxo.getSql(), sqlxo.getParamList().toArray());
    }

}
