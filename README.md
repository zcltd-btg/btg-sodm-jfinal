# btg-sodm-jfinal

#### 项目介绍
> sodm-scope of data management，基于jfinal的配置式权限管理框架。

#### 升级记录
```
v4.0.1
1、转为独立maven依赖，去掉parent；

v3.0.3
1、btg-parent升级到v2.0.1；

v3.0.2
1、统一依赖管理

v3.0.1
1、转为maven项目；
2、统一迁移至公司名下；
```
---
豆圆
